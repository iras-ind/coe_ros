
#ifndef __coe__ros__base_shared_memeory__h__
#define __coe__ros__base_shared_memeory__h__

#include <boost/algorithm/string.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/sharable_lock.hpp>
#include <boost/interprocess/sync/upgradable_lock.hpp>

#include <tuple>
#include <ros/ros.h>

 
#include <realtime_utilities/realtime_utilities.h>
#include <coe_core/coe_utilities.h>


namespace coe_master 
{

/**
 * @class IPC
 * 
 */
class IPC
{

public:
  typedef std::shared_ptr< IPC >  Ptr;
  
  enum ErrorCode  { NONE_ERROR               =  0
                  , UNMACTHED_DATA_DIMENSION =  1
                  , UNCORRECT_CALL           =  2
                  , WATCHDOG                 =  3 };
                  
  struct IPCStruct
  {
    
    struct Header
    {
      uint8_t bond_flag_;
      uint8_t rt_flag_;
      double  time_;
    } __attribute__((packed)) header_;
    
    char    buffer[1024];
    IPCStruct( )
    { 
      clear( );
    }
    void clear( ) 
    {
      header_.bond_flag_ = 0;
      header_.rt_flag_ = 0;
      header_.time_ = 0;
      std::memset( &buffer[0], 0x0, 1024*sizeof(char) );
    }
  };



  
  enum IPCAccessMode { CREATE, OPEN };
  
  IPC(const std::string& identifier, double operational_time, double watchdog_decimation, const IPCAccessMode& mode, const size_t dim );
  IPC(const std::string& identifier, double operational_time, double watchdog_decimation );
  ~IPC();
  
  bool      isHardRT ( );
  bool      setHardRT( );
  bool      setSoftRT( );
  
  bool      isBonded ( );
  bool      bond     ( );
  bool      breakBond( );
  ErrorCode update   ( const uint8_t* buffer, const double time, const size_t& n_bytes );
  ErrorCode flush    ( uint8_t* buffer, double* time, double* latency_time, const size_t& n_bytes );
  
  size_t      getSize( bool prepend_header ) const;
  std::string getName()                      const;
  double      getWatchdog( )                 const;
  std::string to_string( ErrorCode err );
  
  void        dump( IPCStruct* buffer )
  {
    return getIPCStruct(buffer);
  }
  
protected:
  
  const IPCAccessMode                               access_mode_;
  const double                                      operational_time_;
  const double                                      watchdog_;
  
  const std::string                                 name_;
  size_t                                            dim_data_;
  size_t                                            dim_with_header_;
  boost::interprocess::mapped_region                shared_map_;
  boost::interprocess::shared_memory_object         shared_memory_;
  std::shared_ptr<boost::interprocess::named_mutex> mutex_;
  double                                            start_watchdog_time_;
  double                                            data_time_prev_;
  double                                            flush_time_prev_;
  
  size_t                                            bond_cnt_;
  bool                                              bonded_prev_;
  bool                                              is_hard_rt_prev_;
  
  void getIPCStruct( IPCStruct* shmem );
  void setIPCStruct( const IPCStruct* shmem);
};



/**
 * 
 * 
 * 
 * 
 * 
 * 
 */
inline 
IPC::IPC(const std::string& identifier, double operational_time, double watchdog_decimation, const IPCAccessMode& mode, const size_t dim )
: access_mode_         ( mode                 )
, operational_time_    ( operational_time     )
, watchdog_            ( watchdog_decimation  )
, name_                ( identifier  )
, dim_data_            ( dim )
, dim_with_header_     ( dim + sizeof(IPC::IPCStruct::Header) )  //time and bonding index
, start_watchdog_time_ ( -1 )
, data_time_prev_      ( 0  )
, flush_time_prev_     ( 0  )
, bond_cnt_            ( 0  )
, bonded_prev_         ( false )
, is_hard_rt_prev_     ( false )
{
  bool ok = true;
  try
  {
    ROS_DEBUG ( "[%sSTART%s] %sShMem Init%s [ %s%s%s ]", BOLDMAGENTA(), RESET(), BOLDBLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET() );
    if ( dim_data_ == 0 )
    {
      ROS_WARN ( "[%sCHECK%s%s] %sShMem Init%s [ %s%s%s%s ] Memory does not exist, is it correct?"
                , BOLDRED(), RESET(), YELLOW(), BLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET(), YELLOW() );
      return;
    }
    
    //------------------------------------------
    boost::interprocess::permissions permissions ( 0677 );
    if ( access_mode_ == CREATE )
    {
      if ( !boost::interprocess::named_mutex::remove ( name_.c_str() ) )
      {
        ROS_DEBUG ( "[CHECK] %sShMem Init%s [ %s%s%s%s ] Error in Removing Mutex", BLUE(), YELLOW(), BOLDCYAN(), name_.c_str(), RESET(), YELLOW() );
      }

      if ( !boost::interprocess::shared_memory_object::remove ( name_.c_str() ) )
      {
        ROS_DEBUG ( "[CHECK] %sShMem Init%s [ %s%s%s%s ] Error in Removing Shmem", BLUE(), YELLOW(), BOLDCYAN(), name_.c_str(), RESET(), YELLOW() );
      }

      // store old
      mode_t old_umask = umask ( 0 );

      ROS_DEBUG("[-----] %sShMem Init%s [ %s%s%s ] Sizeof uint8 %zu sizeof double %zu sizeof header %zu", BLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET(),  sizeof(uint8_t), sizeof(double), sizeof(IPCStruct::Header) );
      ROS_DEBUG("[-----] %sShMem Init%s [ %s%s%s ] Create memory (bytes %s%zu/%zu%s)", BLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET(),  BOLDCYAN(), dim_data_, dim_with_header_, RESET() );
      shared_memory_ = boost::interprocess::shared_memory_object( boost::interprocess::create_only
                                                                , name_.c_str()
                                                                , boost::interprocess::read_write
                                                                , permissions );

      shared_memory_.truncate ( dim_with_header_ );
      shared_map_ = boost::interprocess::mapped_region ( shared_memory_, boost::interprocess::read_write );

      std::memset ( shared_map_.get_address(), 0, shared_map_.get_size() );

      mutex_.reset ( new  boost::interprocess::named_mutex ( boost::interprocess::create_only, name_.c_str(), permissions ) );

      // restore old
      umask ( old_umask );
      
    }
    else
    {
      ROS_DEBUG("[-----] %sShMem Init%s [ %s%s%s ] Bond to Shared Memory (bytes %s%zu/%zu%s)", BLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET(), BOLDCYAN(), dim_data_, dim_with_header_, RESET() );
      shared_memory_ = boost::interprocess::shared_memory_object( boost::interprocess::open_only
                                                                , name_.c_str()
                                                                , boost::interprocess::read_write );

      shared_map_ = boost::interprocess::mapped_region ( shared_memory_, boost::interprocess::read_write );


      ROS_DEBUG("[-----] %sShMem Init%s [ %s%s%s ] Bond to Mutex", BLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET() );
      mutex_.reset ( new  boost::interprocess::named_mutex ( boost::interprocess::open_only, name_.c_str() ) );

    }
    ROS_DEBUG ( "[%s DONE%s] %sShMem Init%s [ %s%s%s ]", BOLDGREEN(), RESET(), BOLDBLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET() );

  }
  catch ( boost::interprocess::interprocess_exception &e )
  {
    ROS_ERROR_STREAM ( "In processing module '" << identifier << "' got the error: " << e.what() ) ;
    ok = false;
  }
  catch ( std::exception& e )
  {
    ROS_ERROR_STREAM ( "In processing module '" << identifier << "' got the error: " << e.what() );
    ok = false;
  }

  if(!ok)
    throw std::runtime_error("Error in creation of the shared memory. Exit.");
}

inline 
IPC::IPC(const std::string& identifier, double operational_time, double watchdog_decimation )
: access_mode_         ( OPEN                )
, operational_time_    ( operational_time    )
, watchdog_            ( watchdog_decimation )
, name_                ( identifier  )
, dim_data_            ( 0 )
, dim_with_header_     ( 0 + sizeof(IPC::IPCStruct::Header) )  //time and bonding index
, start_watchdog_time_ ( -1 )
, data_time_prev_      ( 0  )
, flush_time_prev_     ( 0  )
, bond_cnt_            ( 0  )
, bonded_prev_         ( false )
, is_hard_rt_prev_     ( false )
{
  bool ok = true;
  try
  {
  
    ROS_DEBUG("[-----] %sShMem Init%s [ %s%s%s ] Check Memory", BLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET() );
    shared_memory_ = boost::interprocess::shared_memory_object  ( boost::interprocess::open_only
                                                                , name_.c_str()
                                                                , boost::interprocess::read_write );

    shared_map_ = boost::interprocess::mapped_region ( shared_memory_, boost::interprocess::read_write );

    ROS_DEBUG("[-----] %sShMem Init%s [ %s%s%s ] Check Mutex", BLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET() );
    mutex_.reset ( new  boost::interprocess::named_mutex ( boost::interprocess::open_only, name_.c_str() ) );

    dim_with_header_ = shared_map_.get_size();
    dim_data_        = shared_map_.get_size() - sizeof(IPC::IPCStruct::Header);

    assert ( dim_data_ > 0 );

    ROS_DEBUG ( "[%sREADY%s] %sShMem Init%s [ %s%s%s ] Ready", BOLDGREEN(), RESET(), BLUE(), RESET(), BOLDCYAN(), name_.c_str(), RESET() );
  }
  catch ( boost::interprocess::interprocess_exception &e )
  {
    if ( e.get_error_code() == boost::interprocess::not_found_error )
    {
      ROS_WARN ( "[ %s%s%s ] Memory does not exist, Check if correct?", BOLDCYAN(), name_.c_str(), YELLOW());
      dim_data_ = 0.0;
      return;
    }
    else
    {
      ROS_ERROR_STREAM ( "In processing module '" << identifier << "' [" << name_ << "] got the error: " << e.what() << " error code: " << e.get_error_code() ) ;
      ok = false;
    }
  }
  catch ( std::exception& e )
  {
    ROS_ERROR_STREAM ( "In processing module '" << identifier << "' got the error: " << e.what() );
    ok = false;
  }

  if(!ok)
    throw std::runtime_error("Error in creation of the shared memory. Exit.");
}


inline
IPC::~IPC()
{
  
  if ( dim_data_ > 0 )
  {
    ROS_DEBUG ( "[ %s%s%s ][ %sShMem Destructor%s ] Shared Mem Destructor", BOLDCYAN(), name_.c_str(), RESET() , BOLDBLUE(), RESET());
    
    if( isBonded() )
      breakBond();
    
    if ( access_mode_ == CREATE )
    {
    
      ROS_DEBUG ( "[ %s%s%s ][ %sShMem Destructor%s ] Remove Shared Mem ", BOLDCYAN(), name_.c_str(), RESET(), BOLDBLUE(), RESET());

      if ( ! boost::interprocess::shared_memory_object::remove ( name_.c_str() ) )
      {
        ROS_ERROR ( "Error in removing the shared memory object" );
      }

      ROS_DEBUG ( "[ %s%s%s ][ %sShMem Destructor%s ] Remove Mutex", BOLDCYAN(), name_.c_str(), RESET(), BOLDBLUE(), RESET());

      if ( !boost::interprocess::named_mutex::remove ( name_.c_str() ) )
      {
        ROS_ERROR ( "[ %s%s%s ][ %sShMem Destructor%s ] Error", BOLDCYAN(), name_.c_str(), RED(), BOLDBLUE(), RESET());
      }
    }
  }
}

inline 
void IPC::getIPCStruct( IPCStruct* shmem )
{
  assert( shmem );
  assert ( shared_map_.get_size() < sizeof(IPC::IPCStruct) );
  
  shmem->clear();
  boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock ( *mutex_ );  // from local buffer to shared memory
  std::memcpy ( shmem, shared_map_.get_address(), shared_map_.get_size() );
  lock.unlock();
  
}

inline 
void IPC::setIPCStruct( const IPCStruct* shmem )
{
  assert( shmem );
  assert ( shared_map_.get_size() < sizeof(IPC::IPCStruct) );
  
  boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock ( *mutex_ );
  std::memcpy ( shared_map_.get_address(), shmem, shared_map_.get_size()  );
  lock.unlock();
}


inline 
bool IPC::isHardRT ( )
{ 
  if( shared_map_.get_size() == 0 )
    return false;
  
  IPCStruct shmem;
  getIPCStruct(&shmem);
  
  bool is_hard_rt = (shmem.header_.rt_flag_ ==  1);
  if( is_hard_rt_prev_ != is_hard_rt )
  {
    ROS_WARN( "[ %s%s%s ] RT State Changed from '%s%s%s' to '%s%s%s'", BOLDCYAN(), name_.c_str(), RESET()
            , BOLDCYAN(), ( is_hard_rt_prev_ ? "HARD" : "SOFT" ), RESET()
            , BOLDCYAN(), ( is_hard_rt       ? "HARD" : "SOFT" ), RESET() ) ;
      is_hard_rt_prev_ = is_hard_rt;
  }
  return (shmem.header_.rt_flag_ ==  1);
  
}


inline 
bool IPC::setHardRT ( )
{ 
  if( shared_map_.get_size() == 0 )
    return false;
  
  ROS_DEBUG( "[ %s%s%s ] [%sSTART%s] Set Hard RT", BOLDCYAN(), name_.c_str(), RESET(), BOLDCYAN(), RESET() ) ;
    
  IPCStruct shmem;
  getIPCStruct(&shmem);
  
  if( (shmem.header_.rt_flag_ ==  1) )
  {
    ROS_WARN( "[ %s%s%s%s ] Already hard RT!", BOLDCYAN(), name_.c_str(), RESET(), RED() ) ;
  }
  
  shmem.header_.rt_flag_ = 1;
  setIPCStruct(&shmem);
  
  ROS_DEBUG( "[ %s%s%s ] [%s DONE%s] Set Hard RT", BOLDCYAN(), name_.c_str(), RESET(), BOLDGREEN(), RESET() ) ;
  return true;
}

inline 
bool IPC::setSoftRT ( )
{ 
  if( shared_map_.get_size() == 0 )
    return false;
  
  ROS_DEBUG( "[ %s%s%s ] [%sSTART%s] Set Soft RT", BOLDCYAN(), name_.c_str(), RESET(), BOLDCYAN(), RESET() ) ;
    
  IPCStruct shmem;
  getIPCStruct(&shmem);
  
  if( (shmem.header_.rt_flag_ ==  0) )
  {
    ROS_DEBUG( "[ %s%s%s%s ] Already soft RT!", BOLDCYAN(), name_.c_str(), RESET(), RED() ) ;
  }
  
  shmem.header_.rt_flag_ = 0;
  setIPCStruct(&shmem);
  
  ROS_DEBUG( "[ %s%s%s ] [%s DONE%s] Set soft RT", BOLDCYAN(), name_.c_str(), RESET(), BOLDGREEN(), RESET() ) ;
  return true;
}



inline 
bool IPC::isBonded ( )
{ 
  if( shared_map_.get_size() == 0 )
    return false;
  
  IPCStruct shmem;
  getIPCStruct(&shmem);
  
  
  bool is_bonded = (shmem.header_.bond_flag_ == 1);
  if( bonded_prev_ != is_bonded )
  {
    ROS_WARN( "[ %s%s%s ] Bonding State Changed from '%s%s%s' to '%s%s%s'", BOLDCYAN(), name_.c_str(), RESET()
            , BOLDCYAN(), ( bonded_prev_  ? "BONDED" : "UNBONDED" ), RESET()
            , BOLDCYAN(), ( is_bonded     ? "BONDED" : "UNBONDED" ), RESET() ) ;
      bonded_prev_ = is_bonded;
  }
  return (shmem.header_.bond_flag_ == 1);
}


inline 
bool IPC::bond ( )
{
  if( shared_map_.get_size() == 0 )
    return false;
  
  ROS_DEBUG( "[ %s%s%s ] [%sSTART%s] Bonding", BOLDCYAN(), name_.c_str(), RESET(), BOLDCYAN(), RESET() ) ;
  
  IPCStruct shmem;
  getIPCStruct(&shmem);
  
  if( (shmem.header_.bond_flag_ ==  1) )
  {
    ROS_ERROR( "[ %s%s%s%s ] Already Bonded! Abort. \n\n****** RESET CMD FOR SAFETTY **** \n", BOLDCYAN(), name_.c_str(), RESET(), RED() ) ;
    return false;
  }
  
  shmem.header_.bond_flag_ = 1;
  setIPCStruct(&shmem);
  
  ROS_DEBUG( "[ %s%s%s ] [%sDONE%s] Bonding", BOLDCYAN(), name_.c_str(), RESET(), BOLDGREEN(), RESET() ) ;
  return true;
}

inline 
bool IPC::breakBond ( )
{ 
  ROS_DEBUG( "[ %s%s%s ] Break Bond", BOLDCYAN(), name_.c_str(), RESET() ) ;
  IPCStruct shmem;
  getIPCStruct(&shmem);
  
  shmem.header_.rt_flag_ = 0;
  shmem.header_.bond_flag_ = 0;
  
  setIPCStruct(&shmem);
  
  return true;
  
}


inline 
IPC::ErrorCode IPC::update ( const uint8_t* ibuffer, double time, const size_t& n_bytes )
{
  if ( dim_data_ != n_bytes )
  {
    ROS_ERROR ( "FATAL ERROR! Shared memory map '%zu' bytes, while the input is of '%zu' bytes", dim_data_, n_bytes );
    return coe_master::IPC::UNMACTHED_DATA_DIMENSION;
  }
  
  if ( dim_data_ <= 0 )
  {
    return coe_master::IPC::NONE_ERROR;
  }
  
  IPCStruct shmem;
  getIPCStruct(&shmem);

  if( shmem.header_.bond_flag_ == 1 )
  {
    shmem.header_.time_ = time;
    std::memcpy( shmem.buffer, ibuffer, dim_data_ );
  }
  
  setIPCStruct(&shmem);

  return coe_master::IPC::NONE_ERROR;
}



inline 
IPC::ErrorCode IPC::flush ( uint8_t* obuffer, double* time, double* latency_time, const size_t& n_bytes )
{
  IPC::ErrorCode ret = coe_master::IPC::NONE_ERROR;
  if( dim_data_ != n_bytes )
  {
    ROS_ERROR ( "FATAL ERROR! Wrong Memory Dimensions." );
    return coe_master::IPC::UNMACTHED_DATA_DIMENSION;
  }
  
  if ( dim_data_ <= 0 ) 
  {
    return coe_master::IPC::NONE_ERROR;
  }
      
  IPCStruct shmem;
  getIPCStruct(&shmem);

  if( shmem.header_.bond_flag_ == 1 )
  {
    *time = shmem.header_.time_;
    std::memcpy( obuffer, shmem.buffer, dim_data_ );
  }
  else
  {
    // ROS_ERROR_THROTTLE( 2, "[ %s%s%s%s ] SAFETTY CMD (not bonded)", BOLDCYAN(), name_.c_str(), RESET(), RED()) ;
    *time = 0.0;
    std::memset ( obuffer, 0x0, dim_data_ );
  }
  
  
  // check
  *latency_time = ( *time - data_time_prev_ );

  struct timespec flush_ts;
  clock_gettime( CLOCK_MONOTONIC, &flush_ts);
  double flush_time = realtime_utilities::timer_to_s( &flush_ts );
  if( ( access_mode_ == OPEN ) && ( std::fabs( flush_time - *time  ) > 2 * watchdog_ ) )
  {
    ROS_WARN( "Data not updated! (%f,%f,%f)!", flush_time, *time, watchdog_ );
    return coe_master::IPC::WATCHDOG;
  }

  if( shmem.header_.bond_flag_ == 1 )  
  {
    /////////////////////////////////////////////////
    if( ( *latency_time < watchdog_)  && ( *latency_time > 1e-5 ) )  // the client cycle time is in the acceptable trange watchdog
    {
      start_watchdog_time_ = -1; 
    }
    else if( *latency_time > watchdog_) 
    {
      ROS_WARN( "Latency overcome the watchdog (%f,%f,%f)!", *latency_time, *time, data_time_prev_ );
      ret = coe_master::IPC::WATCHDOG;
    }
    else if ( *latency_time < 1e-5 ) // the client is not writing new data in the shared memory ...
    {      
      if( start_watchdog_time_ == -1 ) 
      {
        start_watchdog_time_ = flush_time;
      }
      else if( ( flush_time - start_watchdog_time_ ) > watchdog_ )
      {
        ret = coe_master::IPC::WATCHDOG;
      }
    }
    /////////////////////////////////////////////////
    
    /////////////////////////////////////////////////
    if( ret == coe_master::IPC::WATCHDOG )
    {
      if( shmem.header_.rt_flag_==1) 
      {
        ROS_ERROR_THROTTLE( 2, "[ %s%s%s%s ] Watchdog %fms (allowed: %f) ****** RESET CMD FOR SAFETTY ****"
          , BOLDCYAN(), name_.c_str(), RESET(), RED(), ( flush_time - start_watchdog_time_ ), watchdog_ ) ;
        if( access_mode_ == CREATE )
        {
          std::memset ( obuffer, 0x0, dim_data_ );
        }
      }
      else
      {
        ROS_WARN_THROTTLE( 2, "[ %s%s%s%s ] Watchdog %fms (allowed: %f) ****** SOFT RT, DON'T CARE ****"
          , BOLDCYAN(), name_.c_str(), RESET(), YELLOW(), ( flush_time - start_watchdog_time_ ), watchdog_ ) ;
          ret = coe_master::IPC::NONE_ERROR;
      
      }  
    }
    /////////////////////////////////////////////////
  }
  data_time_prev_ = *time;
  flush_time_prev_ = flush_time;

  return ret;
}





inline std::string IPC::to_string( IPC::ErrorCode err )
{ 
  std::string ret = "na";
  switch(err)
  {
    case NONE_ERROR               : ret = "SHARED MEMORY NONE ERROR";               break;
    case UNMACTHED_DATA_DIMENSION : ret = "SHARED MEMORY UNMATHCED DATA DIMENSION"; break;
    case UNCORRECT_CALL           : ret = "SHARED MEMORY UNCORRECT CALL SEQUENCE";  break;
    case WATCHDOG                 : ret = "SHARED MEMORY WATCHDOG";                 break;
  }
  return ret; 
}

inline 
size_t IPC::getSize( bool prepend_header ) const
{ 
  return prepend_header ? dim_with_header_ : dim_data_; 
}

inline 
double IPC::getWatchdog(  ) const
{ 
  return watchdog_; 
}

}

#endif
