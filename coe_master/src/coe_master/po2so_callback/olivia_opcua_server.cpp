/*                                                                   *
 *      _____ ______ _____ _______            _____ _  ___  _        *
 *     / ____|  ____|_   _|__   __|          |_   _| |/ / || |       * 
 *    | |    | |__    | |    | |     ______    | | | ' /| || |_      *
 *    | |    |  __|   | |    | |    |______|   | | |  < |__   _|     *
 *    | |____| |____ _| |_   | |              _| |_| . \   | |       *
 *     \_____|______|_____|  |_|             |_____|_|\_\  |_|       *
 *                                                                   *
 *                              AGV - Server Code                    *
 *                    Using Open62541 and the templates              *
 *                                                                   */

#include <stdio.h>
#include <signal.h>
#include <pthread.h>    // if needed to update the values as simulation case
#include <unistd.h>     // if needed to update the values as simulation case
#include <ros/ros.h>

#include <olivia_opcua_server/olivia_opcua_server.h>
#include <olivia_opcua_server/olivia_opcua_server_callbacks.h>
#include <olivia_opcua_server/open62541/open62541.h>
 
SweepeeOpcUaServer::SweepeeOpcUaServer( ros::NodeHandle& nh) : nh_( nh )
{
  running = false;
  config = NULL; 
  server = NULL; 
  PosX = 0;
  PosY = 0;
  RotZ = 0;
  
  FinalPosX = 0;
  FinalPosY = 0;
  FinalRotZ = 0;
  
  KmH = 0;
  BattCharge = 100;
  IsMoving = UA_FALSE;
  IsValidRoute = UA_TRUE;
  IsBlocked = UA_FALSE;
  Occlusion = 0;
  
  
  config        = UA_ServerConfig_new_minimal(4840,NULL);
  server        = UA_Server_new(config);
  
  parentNodeId          = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
  parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
  
  addVariableAGVid();
  addVariablePos();
  addVariableKhM();
  
  addVariableBattCharge();
  addVariableFinalPos();
  addVariableIsMoving();
  addVariableIsBlocked();
  addVariableIsValidRoute();
  
  addVariableOcclusion();
  addCheckPositions( );
}

SweepeeOpcUaServer::~SweepeeOpcUaServer( )   
{
  UA_Server_delete(server);
  UA_ServerConfig_delete(config);
}


void SweepeeOpcUaServer::run( ) 
{ 
  UA_StatusCode retval = UA_Server_run(server,&running);
}
void SweepeeOpcUaServer::stop()
{
  UA_LOG_INFO(UA_Log_Stdout,UA_LOGCATEGORY_USERLAND,"Ctrl-c: Quit the server");
  running = false;
}

void SweepeeOpcUaServer::posCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  PosX = msg->pose.pose.position.x;
  PosY = msg->pose.pose.position.y;
  tf::Quaternion q;
  tf::quaternionMsgToTF(msg->pose.pose.orientation, q);
  tf::Matrix3x3 m(q);
  double roll, pitch, yaw;
  m.getRPY(roll, pitch, yaw);
  RotZ = yaw;
}

void SweepeeOpcUaServer::goalCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  FinalPosX = msg->pose.position.x;
  FinalPosY = msg->pose.position.z;
  tf::Quaternion q;
  tf::quaternionMsgToTF(msg->pose.orientation, q);
  tf::Matrix3x3 m(q);
  double roll, pitch, yaw;
  m.getRPY(roll, pitch, yaw);
  FinalRotZ = yaw;
}

void SweepeeOpcUaServer::kmCallback(const std_msgs::Float64::ConstPtr& msg)
{
  KmH = msg->data;
}
void SweepeeOpcUaServer::batteryChargeCallback(const std_msgs::Int32::ConstPtr& msg)
{
  BattCharge = msg->data;
}

void SweepeeOpcUaServer::isMovingCallback(const std_msgs::Bool::ConstPtr& msg)
{
  IsMoving = msg->data;
}

void SweepeeOpcUaServer::isValidRouteCallback(const std_msgs::Bool::ConstPtr& msg)
{
  IsValidRoute = msg->data;
}

void SweepeeOpcUaServer::isBlockedCallback(const std_msgs::Bool::ConstPtr& msg)
{
  IsBlocked = msg->data;
}

void SweepeeOpcUaServer::occlusionCallback(const std_msgs::Int32::ConstPtr& msg)
{
  Occlusion = msg->data;
}



// Internal function to update variable value if called
UA_StatusCode SweepeeOpcUaServer::UpdatePos (UA_Server *server,
                        const UA_NodeId *sessionId, void *sessionHandle,
                        const UA_NodeId *methodId, void *methodContext,
                        const UA_NodeId *objectId, void *objectContext,
                        size_t inputSize, const UA_Variant *input,
                        size_t outputSize, UA_Variant *output) 
{

  PosX+=10;
  PosY+=10;
  RotZ+=10;
  float newPos[3] = {PosX,PosY,RotZ};

  UA_StatusCode retval = UA_Variant_setArrayCopy(output,newPos,3, &UA_TYPES[UA_TYPES_FLOAT]);

  if(retval!=UA_STATUSCODE_GOOD)
    return retval;

  UA_Float *outputArray = (UA_Float*)output->data;
  outputArray[0] = newPos[0];
  outputArray[1] = newPos[1];
  outputArray[2] = newPos[2];
  return UA_STATUSCODE_GOOD;
}

// External function to update variable value when called
void SweepeeOpcUaServer::addCheckPositions( ) 
{
  UA_Argument Positions;
  UA_Argument_init(&Positions);
  Positions.description = UA_LOCALIZEDTEXT("en-US", "float[3] array");
  Positions.name = UA_STRING("each entry is incremented by 10");
  Positions.dataType = UA_TYPES[UA_TYPES_FLOAT].typeId;
  Positions.valueRank = 1;
  UA_UInt32 pOutputDimension = 3;
  Positions.arrayDimensionsSize = 1;
  Positions.arrayDimensions = &pOutputDimension;

  UA_MethodAttributes updateAttr = UA_MethodAttributes_default;
  updateAttr.description = UA_LOCALIZEDTEXT("en-US","UpdatePositions");
  updateAttr.displayName = UA_LOCALIZEDTEXT("en-US","UpdatePositions");
  updateAttr.executable = true;
  updateAttr.userExecutable = true;

  UA_Server_addMethodNode( server
                          , UA_NODEID_STRING(1,"UpdatePositions")
                          , UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER)
                          , UA_NODEID_NUMERIC(0, UA_NS0ID_HASORDEREDCOMPONENT)
                          , UA_QUALIFIEDNAME(1, "UpdatePositions")
                          , updateAttr
                          , MemberFunctionCallback(this, &SweepeeOpcUaServer::UpdatePos)
                          , 0
                          , NULL
                          , 1
                          , &Positions
                          , NULL
                          , NULL);
  
}

// Creates the structure of the server variables
void SweepeeOpcUaServer::addVariableAGVid() 
{
  UA_ObjectAttributes oAttr = UA_ObjectAttributes_default;
  oAttr.displayName = UA_LOCALIZEDTEXT("en.US","AGV");
  UA_Server_addObjectNode(server
                        ,UA_NODEID_NULL
                        ,UA_NODEID_NUMERIC(0,UA_NS0ID_OBJECTSFOLDER)
                        ,UA_NODEID_NUMERIC(0,UA_NS0ID_ORGANIZES)
                        ,UA_QUALIFIEDNAME(1,"AGV")
                        ,UA_NODEID_NUMERIC(0,UA_NS0ID_BASEOBJECTTYPE)
                        ,oAttr
                        ,NULL
                        ,&AGVid);
    
}
void SweepeeOpcUaServer::addVariablePos() 
{
  UA_VariableAttributes attr = UA_VariableAttributes_default;
    
  UA_Variant_setScalar(&attr.value, &PosX, &UA_TYPES[UA_TYPES_FLOAT]);
  attr.description = UA_LOCALIZEDTEXT("en-US","Current Position X");
  attr.displayName = UA_LOCALIZEDTEXT("en-US","Current Position X");
  attr.dataType = UA_TYPES[UA_TYPES_FLOAT].typeId;
  attr.accessLevel = UA_ACCESSLEVELMASK_READ ;

  UA_NodeId PosXNodeId            = UA_NODEID_STRING(1, "PosX");
  UA_QualifiedName PosXName       = UA_QUALIFIEDNAME(1, "PosX");
  
  UA_Server_addVariableNode(server, PosXNodeId, AGVid,
                            parentReferenceNodeId, PosXName,
                            UA_NODEID_NUMERIC(0, 
                            UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, 
                            NULL);


// Current Position on Y
  UA_Variant_setScalar(&attr.value, &PosY, &UA_TYPES[UA_TYPES_FLOAT]);
  attr.description = UA_LOCALIZEDTEXT("en-US","Current Position Y");
  attr.displayName = UA_LOCALIZEDTEXT("en-US","Current Position Y");
  attr.dataType = UA_TYPES[UA_TYPES_FLOAT].typeId;
  attr.accessLevel = UA_ACCESSLEVELMASK_READ ;

  UA_NodeId PosYNodeId = UA_NODEID_STRING(1, "PosY");
  UA_QualifiedName PosYName = UA_QUALIFIEDNAME(1, "PosY");
  UA_Server_addVariableNode(server, PosYNodeId, AGVid,
                            parentReferenceNodeId, PosYName,
                            UA_NODEID_NUMERIC(0,
                            UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL,
                            NULL);


  // Current Rotation on Z
  UA_Variant_setScalar(&attr.value, &RotZ, &UA_TYPES[UA_TYPES_FLOAT]);
  attr.description = UA_LOCALIZEDTEXT("en-US","Current Rotation Z");
  attr.displayName = UA_LOCALIZEDTEXT("en-US","Current Rotation Z");
  attr.dataType = UA_TYPES[UA_TYPES_FLOAT].typeId;
  attr.accessLevel = UA_ACCESSLEVELMASK_READ ;

  UA_NodeId RotZNodeId = UA_NODEID_STRING(1, "RotZ");
  UA_QualifiedName RotZName = UA_QUALIFIEDNAME(1, "RotZ");
  UA_Server_addVariableNode(server, RotZNodeId, AGVid,
                            parentReferenceNodeId, RotZName,
                            UA_NODEID_NUMERIC(0,
                            UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, 
                            NULL);
  
  sub_pos = nh_.subscribe<nav_msgs::Odometry>(DEFAUL_NS_Pos,1, &SweepeeOpcUaServer::posCallback, this );
  
}

void SweepeeOpcUaServer::addVariableFinalPos() 
{
  UA_VariableAttributes attr = UA_VariableAttributes_default;

  // Final Position on X
  UA_Variant_setScalar(&attr.value, &FinalPosX, &UA_TYPES[UA_TYPES_FLOAT]);
  attr.description = UA_LOCALIZEDTEXT("en-US","Final Position X");
  attr.displayName = UA_LOCALIZEDTEXT("en-US","Final Position X");
  attr.dataType = UA_TYPES[UA_TYPES_FLOAT].typeId;
  attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

  UA_NodeId FinalPosXNodeId = UA_NODEID_STRING(1, "FinalPosX");
  UA_QualifiedName FinalPosXName = UA_QUALIFIEDNAME(1, "FinalPosX");
  UA_Server_addVariableNode(server, FinalPosXNodeId, AGVid,
                            parentReferenceNodeId, FinalPosXName,
                            UA_NODEID_NUMERIC(0, 
                            UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, 
                            NULL);
  
  // Final Position on Y
  UA_Variant_setScalar(&attr.value, &FinalPosY, &UA_TYPES[UA_TYPES_FLOAT]);
  attr.description = UA_LOCALIZEDTEXT("en-US","Final Position Y");
  attr.displayName = UA_LOCALIZEDTEXT("en-US","Final Position Y");
  attr.dataType = UA_TYPES[UA_TYPES_FLOAT].typeId;
  attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

  UA_NodeId FinalPosYNodeId = UA_NODEID_STRING(1, "FinalPosY");
  UA_QualifiedName FinalPosYName = UA_QUALIFIEDNAME(1, "FinalPosY");
  UA_Server_addVariableNode(server, FinalPosYNodeId, AGVid,
                            parentReferenceNodeId, FinalPosYName,
                            UA_NODEID_NUMERIC(0, 
                            UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, 
                            NULL);

  // Final Rotation on Z;
  UA_Variant_setScalar(&attr.value, &FinalRotZ, &UA_TYPES[UA_TYPES_FLOAT]);
  attr.description = UA_LOCALIZEDTEXT("en-US","Final Rotation Z");
  attr.displayName = UA_LOCALIZEDTEXT("en-US","Final Rotation Z");
  attr.dataType = UA_TYPES[UA_TYPES_FLOAT].typeId;
  attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

  UA_NodeId FinalRotZNodeId = UA_NODEID_STRING(1, "FinalRotZ");
  UA_QualifiedName FinalRotZName = UA_QUALIFIEDNAME(1, "FinalRotZ");
  UA_Server_addVariableNode(server, FinalRotZNodeId, AGVid,
                            parentReferenceNodeId, FinalRotZName,
                            UA_NODEID_NUMERIC(0, 
                            UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, 
                            NULL);
  
  sub_goal = nh_.subscribe<geometry_msgs::PoseStamped>(DEFAUL_NS_Pos,1, &SweepeeOpcUaServer::goalCallback, this );
}

void SweepeeOpcUaServer::addVariableKhM() 
{
  UA_VariableAttributes attr = UA_VariableAttributes_default;

  // Speed on Kilometers per Hour
  UA_Variant_setScalar(&attr.value, &KmH, &UA_TYPES[UA_TYPES_FLOAT]);
  attr.description = UA_LOCALIZEDTEXT("en-US","Kilometers Per Hour");
  attr.displayName = UA_LOCALIZEDTEXT("en-US","Kilometers Per Hour");
  attr.dataType = UA_TYPES[UA_TYPES_FLOAT].typeId;
  attr.accessLevel = UA_ACCESSLEVELMASK_READ ;

  UA_NodeId KmHNodeId = UA_NODEID_STRING(1, "KmH");
  UA_QualifiedName KmHName = UA_QUALIFIEDNAME(1, "KmH");
  UA_Server_addVariableNode(server, KmHNodeId, AGVid,
                            parentReferenceNodeId, KmHName,
                            UA_NODEID_NUMERIC(0, 
                            UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, 
                            NULL);
  
  sub_kmh = nh_.subscribe<std_msgs::Float64>(DEFAUL_NS_Pos,1, &SweepeeOpcUaServer::kmCallback, this );
}

void SweepeeOpcUaServer::addVariableBattCharge() 
{
  UA_VariableAttributes attr = UA_VariableAttributes_default;
    // Battery charge, integer from 0 to 100, implement the range in the device
    UA_Variant_setScalar(&attr.value, &BattCharge, &UA_TYPES[UA_TYPES_BYTE]);
    attr.description = UA_LOCALIZEDTEXT("en-US","Battery remaining charge");
    attr.displayName = UA_LOCALIZEDTEXT("en-US","Battery remaining charge");
    attr.dataType = UA_TYPES[UA_TYPES_BYTE].typeId;
    attr.accessLevel = UA_ACCESSLEVELMASK_READ ;

    UA_NodeId BattChargeNodeId = UA_NODEID_STRING(1, "BattCharge");
    UA_QualifiedName BattChargeName = UA_QUALIFIEDNAME(1, "BattCharge");
    UA_Server_addVariableNode(server, BattChargeNodeId, AGVid,
                              parentReferenceNodeId, BattChargeName,
                              UA_NODEID_NUMERIC(0, 
                              UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, 
                              NULL);
    
    sub_battery_charge = nh_.subscribe<std_msgs::Int32>(DEFAUL_NS_Pos,1, &SweepeeOpcUaServer::batteryChargeCallback, this );
}

void SweepeeOpcUaServer::addVariableIsMoving() 
{
  UA_VariableAttributes attr = UA_VariableAttributes_default;    
  // Is currently moving the AGV?
    UA_Variant_setScalar(&attr.value, &IsMoving, &UA_TYPES[UA_TYPES_BOOLEAN]);
    attr.description = UA_LOCALIZEDTEXT("en-US","Is the AGV moving");
    attr.displayName = UA_LOCALIZEDTEXT("en-US","Is the AGV moving");
    attr.dataType = UA_TYPES[UA_TYPES_BOOLEAN].typeId;
    attr.accessLevel = UA_ACCESSLEVELMASK_READ ;

    UA_NodeId IsMovingNodeId = UA_NODEID_STRING(1, "IsMoving");
    UA_QualifiedName IsMovingName = UA_QUALIFIEDNAME(1, "IsMoving");
    UA_Server_addVariableNode(server, IsMovingNodeId, AGVid,
                              parentReferenceNodeId, IsMovingName,
                              UA_NODEID_NUMERIC(0, 
                              UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, 
                              NULL);
    sub_is_moving = nh_.subscribe<std_msgs::Bool>(DEFAUL_NS_Pos,1, &SweepeeOpcUaServer::isMovingCallback, this );
}

void SweepeeOpcUaServer::addVariableIsValidRoute() 
{
  UA_VariableAttributes attr = UA_VariableAttributes_default;    
  
    // Is the route valid for the AGV?
    UA_Variant_setScalar(&attr.value, &IsValidRoute, &UA_TYPES[UA_TYPES_BOOLEAN]);
    attr.description = UA_LOCALIZEDTEXT("en-US","The route is valid");
    attr.displayName = UA_LOCALIZEDTEXT("en-US","The route is valid");
    attr.dataType = UA_TYPES[UA_TYPES_BOOLEAN].typeId;
    attr.accessLevel = UA_ACCESSLEVELMASK_READ ;

    UA_NodeId IsValidRouteNodeId = UA_NODEID_STRING(1, "IsValidRoute");
    UA_QualifiedName IsValidRouteName = UA_QUALIFIEDNAME(1, "IsValidRoute");
    UA_Server_addVariableNode(server, IsValidRouteNodeId, AGVid,
                              parentReferenceNodeId, IsValidRouteName,
                              UA_NODEID_NUMERIC(0, 
                              UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, 
                              NULL);
  sub_is_valid = nh_.subscribe<std_msgs::Bool>(DEFAUL_NS_Pos,1, &SweepeeOpcUaServer::isValidRouteCallback, this );
}

void SweepeeOpcUaServer::addVariableIsBlocked() 
{
  UA_VariableAttributes attr = UA_VariableAttributes_default;    
    // Is the AGV blocked for something?
    UA_Variant_setScalar(&attr.value, &IsBlocked, &UA_TYPES[UA_TYPES_BOOLEAN]);
    attr.description = UA_LOCALIZEDTEXT("en-US","Something is blocking the way");
    attr.displayName = UA_LOCALIZEDTEXT("en-US","Something is blocking the way");
    attr.dataType = UA_TYPES[UA_TYPES_BOOLEAN].typeId;
    attr.accessLevel = UA_ACCESSLEVELMASK_READ ;

    UA_NodeId IsBlockedNodeId = UA_NODEID_STRING(1, "IsBlocked");
    UA_QualifiedName IsBlockedName = UA_QUALIFIEDNAME(1, "IsBlocked");
    UA_Server_addVariableNode(server, IsBlockedNodeId, AGVid,
                              parentReferenceNodeId, IsBlockedName,
                              UA_NODEID_NUMERIC(0, 
                              UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, 
                              NULL);
    sub_is_blocked = nh_.subscribe<std_msgs::Bool>(DEFAUL_NS_Pos,1, &SweepeeOpcUaServer::isBlockedCallback, this );
}

void SweepeeOpcUaServer::addVariableOcclusion() 
{

  UA_VariableAttributes attr = UA_VariableAttributes_default;    
/*  Is a sensor occluded?
//  0 - None
//  1 - Camera
//  2 - Movement
//  3 - IR */
  UA_Variant_setScalar(&attr.value, &Occlusion, &UA_TYPES[UA_TYPES_BYTE]);
  attr.description = UA_LOCALIZEDTEXT("en-US","A sensor is occluded");
  attr.displayName = UA_LOCALIZEDTEXT("en-US","A sensor is occluded");
  attr.dataType = UA_TYPES[UA_TYPES_BYTE].typeId;
  attr.accessLevel = UA_ACCESSLEVELMASK_READ ;

  UA_NodeId OcclusionNodeId = UA_NODEID_STRING(1, "Occlusion");
  UA_QualifiedName OcclusionName = UA_QUALIFIEDNAME(1, "Occlusion");
  UA_Server_addVariableNode(server, OcclusionNodeId, AGVid,
                            parentReferenceNodeId, OcclusionName,
                            UA_NODEID_NUMERIC(0, 
                            UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, 
                            NULL);

  sub_occlusion = nh_.subscribe<std_msgs::Int32>(DEFAUL_NS_Pos,1, &SweepeeOpcUaServer::occlusionCallback, this );
  
}


