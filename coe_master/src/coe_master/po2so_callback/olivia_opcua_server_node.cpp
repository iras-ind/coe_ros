/*                                                                   *
 *      _____ ______ _____ _______            _____ _  ___  _        *
 *     / ____|  ____|_   _|__   __|          |_   _| |/ / || |       * 
 *    | |    | |__    | |    | |     ______    | | | ' /| || |_      *
 *    | |    |  __|   | |    | |    |______|   | | |  < |__   _|     *
 *    | |____| |____ _| |_   | |              _| |_| . \   | |       *
 *     \_____|______|_____|  |_|             |_____|_|\_\  |_|       *
 *                                                                   *
 *                              AGV - Server Code                    *
 *                    Using Open62541 and the templates              *
 *                                                                   */

#include <stdio.h>
#include <signal.h>
#include <pthread.h>    // if needed to update the values as simulation case
#include <unistd.h>     // if needed to update the values as simulation case
#include <olivia_opcua_server/open62541/open62541.h>

#include <olivia_opcua_server/olivia_opcua_server.h>
#include <olivia_opcua_server/olivia_opcua_server_callbacks.h>

SweepeeOpcUaServer::Ptr opcua_server;


void stopHandler(int sig)
{
  opcua_server->stop();
}



int main(int argc, char* argv[] )
{
  ros::init(argc, argv, "olivia_opcua_server_node", ros::init_options::NoSigintHandler );
  signal(SIGINT,  stopHandler);
  signal(SIGTERM, stopHandler);

  ros::NodeHandle nh("~");
  ros::AsyncSpinner spinner(4);
  
  spinner.start();

  opcua_server.reset( new SweepeeOpcUaServer( nh ) ) ;
  
  opcua_server->run();
  
  ros::waitForShutdown();
  return 1;
}
