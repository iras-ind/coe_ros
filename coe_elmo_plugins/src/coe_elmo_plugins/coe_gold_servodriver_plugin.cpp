#include <stdexcept>  
#include <boost/algorithm/string.hpp>

#include <pluginlib/class_list_macros.hpp>
#include <diagnostic_msgs/DiagnosticStatus.h>
#include <diagnostic_msgs/KeyValue.h>
#include <diagnostic_aggregator/analyzer.h>
#include <diagnostic_aggregator/status_item.h>
#include <diagnostic_aggregator/generic_analyzer_base.h>

#include <coe_driver/diagnostic/coe_generic_analyzer_imp.hpp>

#include <coe_core/coe_string_utilities.h>


namespace coe_elmo_plugins
{

inline std::map<int16_t, std::string > elmoErrorMap() 
{
  std::map<int16_t, std::string > ret = 
  { {0x8110, "CAN message lost (corrupted or overrun)"    }
  , {0x8200, "Protocol error (unrecognized NMT request)"  }
  , {0x8210, "Attempt to access an unconfigured RPDO"     }
  , {0x8130, "Heartbeat event"                            }
  , {0x6180, "Fatal CPU error: stack overflow"            }
  , {0x6200, "User program aborted by an error"           }
  , {0xFF01, "Request by user program “emit” function"    } 
  , {0x6300, "Object mapped to an RPDO returned an error during interpretation or a referenced motion failed to be performed."  }
  , {0xFF02, "DS 402 IP Underflow" }                                                      
  , {0x7300, "Resolver or Analog Encoder feedback failed " } 
  , {0x7305, "Reserved " } 
  , {0x7306, "Reserved " } 
  , {0x7380, "Feedback loss: no match between encoder and Hall locations. Available in encoder + Hall feedback systems " } 
  , {0x8311, "Peak current has been exceeded due to: Drive malfunction; Badly-tuned current controller " } 
  , {0x5441, "Disabled by Limit switch " }
  , {0x5442, "Disable by switch that is defined as external abort" } 
  , {0x5280, "ECAM table problem " } 
  , {0x7381, "Two digital Hall sensors changed at once; only one sensor can be changed at a time. " } 
  , {0x8480, "Speed tracking error DV[2]-VX (for UM=2, 4 or 5) exceeded speed error limit ER[2], due to: Bad tuning of speed controller; Too tight a speed-error tolerance; Inability of motor to accelerate to required speed because line voltage is too low, or motor is not powerful enough" } 
  , {0x8611, " Position tracking error DV[3]-PX (UM5) or DV[3]-PY (UM=4) exceeded position error limit ER[3], due to: Bad tuning of position or speed controller; Too tight a position error tolerance; Abnormal motor load, a mechanical limit reached" } 
  , {0x6320, "Cannot start due to inconsistent database. This type of database inconsistency is reflected in status SR report and in CD CPU dump report. " } 
  , {0x5280, "Too large a difference in ECAM table entries. " } 
  , {0x8130, "Heartbeat failure, occurring only if drive is set to'Abort under heartbeat failure' in a CANopen network (object 0x6007 in CAN object dictionary set to 1, malfunction). " } 
  , {0x8380, "Cannot find electrical zero of motor when attempting to start motor with an incremental encoder and no digital Hall sensors. Applied motor current may not suffice for moving motor from its place. " } 
  , {0x8481, "Speed limit exceeded: VX < LL[2] or VX > HL[2] " } 
  , {0x6180, "Stack overflow: fatal exception. May occur if CPU cannot handle a real-time load due to too low a sampling time. " } 
  , {0x6181, "CPU exception: fatal exception. " } 
  , {0x5281, "Timing Error " } 
  , {0x7121, "Motor stuck: motor powered but not moving according to definition of CL[2] and CL[3]. " } 
  , {0x8680, "Position limit exceeded: PX < LL[3] or PX > HL[3] (UM=5), or PY < LL[3] or PY > HL[3] (UM=4). " } 
  , {0x1000, "Reserved " } 
  , {0x8381, "Cannot tune current offsets " } 
  , {0xFF10, "Cannot start motor" } 
  , {0xFF00, "Queue is low. Number of yet unexecuted PVT table rows has dropped below the value stated in MP[4]\n\
    OR\n\
    Write pointer is out of physical range ([1...64]) of PVT table. Reason may be an improper setting of MP[6].\n\
    OR\n\
    Reserved for Compatibility reason\n\
    OR\n\
    An attempt has been made to program more PVT points than are available in queue.\n\
    OR\n\
    Cannot initialize motion due to bad setup data. The write pointer is outside the range specified by the start and end pointers.\n\
    OR\n\
    Mode terminated and motor has been automatically stopped (in MO=1).\n\
    OR\n\
    A CAN message has been lost." } 
  , {0xFF01, "Request by user program “emit” function" } 
  , {0xFF20, "Safety switch is sensed – drive in safety state" } 
  , {0x8680, "Reserved " } 
  , {0x8312, "Reserved " } 
  , {0x5400, "Cannot start motor" } 
  , {0x3120, "Under-voltage: power supply is shut down or it has too high an output impedance. " } 
  , {0x3310, "Over-voltage: power-supply voltage is too high or servo drive could not absorb kinetic energy while braking a load. A shunt resistor may be required." } 
  , {0x3100, "Reserved " } 
  , {0x2311, "Reserved " } 
  , {0x2340, "Short circuit: motor or its wiring may be defective, or drive is faulty. " } 
  , {0x4310, "Temperature: drive overheating. The environment is too hot or heat removal is not efficient. Could be due to large thermal resistance between drive and its mounting. " } 
  , {0x5282, "Reserved " } 
  };
  return ret;
}

  
  
class GoldServoDriverError : public coe_driver::CoeGenericAnalyzer
{
private:
  
  std::map<int16_t, std::string > error_map_;
  
  std::string to_string(uint32_t err, bool* ok = nullptr ) 
  {
    std::string ret = coe_core::to_string_hex(err) + " not mapped";
    try
    {
      
      auto it = error_map_.find( err );
      bool exist = (it != error_map_.end());
      if( exist ) 
      {
        ret = error_map_.at( err );
      }
      
      if( ok != nullptr ) 
      {
        *ok = exist;
      }
    }
    catch(std::exception& e)
    {
      ROS_ERROR("Error in parsing the error code %u", err);
      return "ERROR!!!";
    }
    return ret;    
  }
  
public:
  
  inline bool analyze(const boost::shared_ptr<diagnostic_aggregator::StatusItem> item)
  {
    try
    {
      boost::shared_ptr<diagnostic_msgs::DiagnosticStatus> diag_status = item->toStatusMsg("ElmoPlugin");
      if( diag_status->values.size() )
      {
        for( size_t j=0; j< diag_status->values.size(); j++ ) 
        {
          
          std::string  key = diag_status->values[j].key;
          std::string  val = diag_status->values[j].value;
          std::string  new_val;
          bool ok=false;
          try
          {
            uint32_t err = std::stoul( key, nullptr, 16 );
            std::vector<std::string> strs;
            boost::split(strs,val,boost::is_any_of("[]"));
            if( strs.size() > 1 )
              new_val = "[" + strs[0] + "] ";
            
            new_val += to_string(err, &ok);
          }
          catch( std::invalid_argument& e )
          {
            new_val = val;
            ok = false;
          }
          
          if( ok )
            diag_status->values[j].value = new_val;
        }
        
        boost::shared_ptr<diagnostic_aggregator::StatusItem> new_item( new diagnostic_aggregator::StatusItem(diag_status.get()) );
        return coe_driver::CoeGenericAnalyzer::analyze( new_item );
      }
      else
        return true;
    }
    catch(std::exception& e)
    {
      ROS_FATAL("GoldServoDriverError::analyze caught an exception.");
    }
    
    return false;
  }

  GoldServoDriverError() : CoeGenericAnalyzer( )
  {
    ROS_INFO( "**********************************************");
    ROS_INFO( "* ELMO ERROR ANALYZER ACTIVE                  ");
    ROS_INFO( "**********************************************");
    error_map_ = elmoErrorMap(); 
  };
  virtual ~GoldServoDriverError() {};
};


PLUGINLIB_EXPORT_CLASS(coe_elmo_plugins::GoldServoDriverError, diagnostic_aggregator::Analyzer );


}
