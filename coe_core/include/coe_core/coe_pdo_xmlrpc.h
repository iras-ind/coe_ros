#ifndef __ITIA_ROS_COE_UTILS__PDO__XMLRPC__H__
#define __ITIA_ROS_COE_UTILS__PDO__XMLRPC__H__

#include <mutex>
#include <yaml-cpp/yaml.h>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>

#include <ros/ros.h>
#include <geometry_msgs/Pose.h>
#include <XmlRpc.h>

#include <rosparam_utilities/rosparam_utilities.h>
//#include <soem/ethercattype.h>
//#include <coe_core/coe_utilities.h>
#include <coe_core/coe_string_utilities.h>
#include <coe_core/coe_pdo.h>

namespace coe_core
{ 

namespace XmlRpcPdo
{

  static const char* KeysId[7]= { "pdo_subindex"
                                , "value_id"
                                , "value_index"
                                , "value_subindex"
                                , "value_type"
                                , "byte_offest"
                                , "bit_offest"
                                };
                                
  enum KeysCode                 { PDO_SUBINDEX = 0
                                , VALUE_NAME
                                , VALUE_INDEX
                                , VALUE_SUBINDEX
                                , VALUE_TYPE
                                , BYTE_OFFSET
                                , BIT_OFFSET };
                        
  
  inline void fromXmlRpcValue( const XmlRpc::XmlRpcValue& node, coe_core::Pdo& pdo, const std::string& log  )
  {
    
    XmlRpc::XmlRpcValue config( node );
    if ( config.getType() != XmlRpc::XmlRpcValue::TypeArray )
    {
      ROS_ERROR ( "The node  is not of type array. %d/%d", int( config.getType()), int( XmlRpc::XmlRpcValue::TypeArray ) );
      throw std::runtime_error("The node  is not of type array");
    }
    
    pdo.clear();
    
    std::map< uint16_t, coe_core::DataObject > objs;
    for(size_t i=0; i < size_t( config.size() ); i++) 
    {
      coe_core::BaseDataObjectEntryPtr obj;
      std::string   name            = rosparam_utilities::toString( config[i], KeysId[ VALUE_NAME  ] ,    log + ", " + std::to_string(i)+"# name");
      int           value_index     = rosparam_utilities::toInt   ( config[i], KeysId[ VALUE_INDEX ] ,    log + ", " + std::to_string(i)+"# value_index");
      int           value_subindex  = rosparam_utilities::toInt   ( config[i], KeysId[ VALUE_SUBINDEX ] , log + ", " + std::to_string(i)+"# value_subindex");
      std::string   value_type      = rosparam_utilities::toString( config[i], KeysId[ VALUE_TYPE ] ,     log + ", " + std::to_string(i)+"# value_type");
    
      switch( coe_core::getType( value_type )  )
      {
        case ECT_BOOLEAN:    obj.reset( new coe_core::DataObjectEntry<bool       >( value_index, value_subindex, name.c_str() ) );   break;
        case ECT_INTEGER8:   obj.reset( new coe_core::DataObjectEntry<int8_t     >( value_index, value_subindex, name.c_str() ) );   break;
        case ECT_INTEGER16:  obj.reset( new coe_core::DataObjectEntry<int16_t    >( value_index, value_subindex, name.c_str() ) );   break;
        case ECT_INTEGER32:  obj.reset( new coe_core::DataObjectEntry<int32_t    >( value_index, value_subindex, name.c_str() ) );   break;
        case ECT_INTEGER64:  obj.reset( new coe_core::DataObjectEntry<int64_t    >( value_index, value_subindex, name.c_str() ) );   break;
        case ECT_UNSIGNED8:  obj.reset( new coe_core::DataObjectEntry<uint8_t    >( value_index, value_subindex, name.c_str() ) );   break;
        case ECT_UNSIGNED16: obj.reset( new coe_core::DataObjectEntry<uint16_t   >( value_index, value_subindex, name.c_str() ) );   break;
        case ECT_UNSIGNED32: obj.reset( new coe_core::DataObjectEntry<uint32_t   >( value_index, value_subindex, name.c_str() ) );   break;
        case ECT_UNSIGNED64: obj.reset( new coe_core::DataObjectEntry<uint64_t   >( value_index, value_subindex, name.c_str() ) );   break;
        case ECT_REAL32:     obj.reset( new coe_core::DataObjectEntry<double     >( value_index, value_subindex, name.c_str() ) );   break;
        case ECT_REAL64:     obj.reset( new coe_core::DataObjectEntry<long double>( value_index, value_subindex, name.c_str() ) );   break;
        case ECT_BIT1:       obj.reset( new coe_core::DataObjectEntry<bool       >( value_index, value_subindex, name.c_str() ) );   break;
        case ECT_BIT2:       obj.reset( new coe_core::DataObjectEntry<bool       >( value_index, value_subindex, name.c_str() ) );   break;
        case ECT_BIT3:       obj.reset( new coe_core::DataObjectEntry<bool       >( value_index, value_subindex, name.c_str() ) );   break;
        case ECT_BIT4:       obj.reset( new coe_core::DataObjectEntry<bool       >( value_index, value_subindex, name.c_str() ) );   break;
        case ECT_BIT5:       obj.reset( new coe_core::DataObjectEntry<bool       >( value_index, value_subindex, name.c_str() ) );   break;
        case ECT_BIT6:       obj.reset( new coe_core::DataObjectEntry<bool       >( value_index, value_subindex, name.c_str() ) );   break;
        case ECT_BIT7:       obj.reset( new coe_core::DataObjectEntry<bool       >( value_index, value_subindex, name.c_str() ) );   break;
        case ECT_BIT8:       obj.reset( new coe_core::DataObjectEntry<bool       >( value_index, value_subindex, name.c_str() ) );   break;
        default:             throw std::runtime_error("Type not yet implemented.");
      }
      pdo.push_back( obj );
    
      if( ( config[i].hasMember( KeysId[ BIT_OFFSET  ] ) ) && ( config[i].hasMember( KeysId[ BYTE_OFFSET ] ) ) ) 
      {
        uint32_t wd = 0; wd  = ((uint32_t)value_index  << 16) | (uint32_t)value_subindex ;
        pdo.start_bytes_map_[ wd ] =  rosparam_utilities::toInt ( config[i][ KeysId[ BYTE_OFFSET ] ] );
        pdo.start_bits_map_ [ wd ] =  rosparam_utilities::toInt ( config[i][ KeysId[ BIT_OFFSET  ] ] );
        pdo.size_bits_map_  [ wd ] =  obj->sizeBits();
      }
    }
  }
    
  inline void toXmlRpcValue( const coe_core::Pdo& pdo, XmlRpc::XmlRpcValue& xml_value  )
  {
    size_t        pdo_subindex  = 0;
    const size_t  npdo          = pdo.nEntries( );
    xml_value.setSize( npdo );
    
    for( auto cob = pdo.begin(); cob != pdo.end(); cob++, pdo_subindex++ )
    {
      rosparam_utilities::toXmlRpcValue((int)(pdo_subindex + 1)                       , xml_value[ pdo_subindex ][ XmlRpcPdo::KeysId[ PDO_SUBINDEX    ] ] );
      rosparam_utilities::toXmlRpcValue((*cob)->name()                                , xml_value[ pdo_subindex ][ XmlRpcPdo::KeysId[ VALUE_NAME      ] ] );
      rosparam_utilities::toXmlRpcValue( (*cob)->index()                              , xml_value[ pdo_subindex ][ XmlRpcPdo::KeysId[ VALUE_INDEX     ] ], "hex" );
      rosparam_utilities::toXmlRpcValue((*cob)->subindex()                            , xml_value[ pdo_subindex ][ XmlRpcPdo::KeysId[ VALUE_SUBINDEX  ] ] );
      rosparam_utilities::toXmlRpcValue(coe_core::dtype2string((*cob)->type(),false)  , xml_value[ pdo_subindex ][ XmlRpcPdo::KeysId[ VALUE_TYPE      ] ] );
      uint32_t wd = 0; wd  = ((uint32_t)(*cob)->index()  << 16) | (uint32_t)(*cob)->subindex();
      
      rosparam_utilities::toXmlRpcValue( (int)( pdo.start_bytes_map_.at( wd ) ) , xml_value[ pdo_subindex ][ XmlRpcPdo::KeysId[ BYTE_OFFSET     ] ], "hex" );
      rosparam_utilities::toXmlRpcValue( (int)( pdo.start_bits_map_.at ( wd ) ) , xml_value[ pdo_subindex ][ XmlRpcPdo::KeysId[ BIT_OFFSET      ] ] );
    }

  }

};







                                              
  

}
#endif





