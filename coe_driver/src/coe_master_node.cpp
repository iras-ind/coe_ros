#include <iostream>
#include <algorithm>
#include <chrono>
#include <cinttypes>
#include <csignal>
#include <boost/thread.hpp>
#include <boost/bind.hpp>

#include <soem/ethercat.h>
#include <soem/ethercattype.h>
#include <soem/ethercatdc.h>

#include <ros/ros.h>
#include <std_srvs/Trigger.h>
#include <diagnostic_updater/publisher.h>
#include <diagnostic_updater/diagnostic_updater.h>

#include <coe_core/coe_sdo.h>
#include <coe_core/coe_utilities.h>
#include <coe_master/coe_master.h>

typedef std::map< ec_err_type,  std::shared_ptr< diagnostic_updater::FunctionDiagnosticTask > > TypedFunctionDiagnosticTask;
typedef std::map< int, TypedFunctionDiagnosticTask > NodesTypedFunctionDiagnosticTask;


class CoeDriverNode
{
private:
  coe_master::CoeMaster   master_;

  ros::NodeHandle         node_handle_;
  ros::NodeHandle         private_node_handle_;

  diagnostic_updater::Updater diagnostic_;

  std::string connect_fail_;

  double desired_freq_;

  std::shared_ptr<SdoManager>           sdo_;
  std::shared_ptr<ros::ServiceServer>   reset_error_service_;

  bool resetErrors( std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res )
  {
    return master_.resetErrors();
  }

  void prepareDiagnostics()
  {
    diagnostic_.add("RT Communication", boost::bind(&CoeDriverNode::nodeDiagnostics     , this, _1));
    diagnostic_.add("SDO Queries"     , boost::bind(&CoeDriverNode::sdoQueryDiagnostics , this, _1));
  }

  void nodeDiagnostics(diagnostic_updater::DiagnosticStatusWrapper& status)
  {
    std::string t = " [" + boost::posix_time::to_simple_string( ros::Time::now().toBoost() ) + "] ";
    if (master_.getState() == master_.CLOSED)       status.summary(diagnostic_msgs::DiagnosticStatus::ERROR, "Not connected. " + connect_fail_ + t);
    else if (master_.getState() == master_.RUNNING) status.summary(diagnostic_msgs::DiagnosticStatus::OK, "Streaming" + t);
    else if (master_.getState() == master_.OPENED)  status.summary(diagnostic_msgs::DiagnosticStatus::OK, "Open");
    else                                            status.summary(diagnostic_msgs::DiagnosticStatus::ERROR, "Unknown state"+ t);

    if( master_.getData() == NULL )
    {
      ROS_WARN("Data not yet in the queue");
      return;
    }
    status.add("Window Time           [s]", master_.getData()->getWindowDim() * master_.getData()->getCycleTime() );
    status.add("[MASTER] Mean Missed Messages  [-]", master_.getData()->getMeanMissedCycles() );
    status.add("[MASTER] Max Missed Messages   [-]", master_.getData()->getMaxMissedCycles() );
    status.add("[MASTER] Mean Calc Time       [ms]", master_.getData()->getMeanCalcTime() );
    status.add("[MASTER] Max Calc Time        [ms]", master_.getData()->getMaxCalcTime() );
    status.add("[MASTER] Mean Wkc              [-]", master_.getData()->getMeanWkc() );
    status.add("[MASTER] Max Wkc               [-]", master_.getData()->getMaxWkc() );
    status.add("[CLIENT] Mean latency         [ms]", master_.getData()->getMeanLatencyTime() );
    status.add("[CLIENT] Max latency          [ms]", master_.getData()->getMaxLatencyTime() );
    auto rxbond = master_.getData()->getRxBonded();
    for( auto const & b : rxbond )
      status.add(std::string( "RxBonded [" + b.first + "]" ).c_str(), b.second );
    
    auto txbond = master_.getData()->getTxBonded();
    for( auto const & b : txbond )
      status.add(std::string( "TxBonded [" + b.first + "]" ).c_str(), b.second );

    auto rxhardrt = master_.getData()->getRxHardRT();
    for( auto const & b : rxhardrt )
      status.add(std::string( "RxHardRT [" + b.first + "]" ).c_str(), b.second );
    
    auto txhardrt = master_.getData()->getRxHardRT();
    for( auto const & b : txhardrt )
      status.add(std::string( "TxHardRT[" + b.first + "]" ).c_str(), b.second );

  }
  
  void sdoQueryDiagnostics(diagnostic_updater::DiagnosticStatusWrapper& status)
  {
    std::string t  = " [" + boost::posix_time::to_simple_string( ros::Time::now().toBoost() ) + "] ";
    
    int cnt_notok = 0; 
    if( sdo_ )
    {
      for( auto sdo_query : sdo_->getQueries( ) )
      {
        status.add( std::get<0>(sdo_query), std::get<1>(sdo_query));
        if( ! std::get<2>(sdo_query) )
          cnt_notok++;
      }
    }
    
    if( cnt_notok == 0 ) 
      status.summary(diagnostic_msgs::DiagnosticStatus::OK, "None Issue " + t);
    else
      status.summary(diagnostic_msgs::DiagnosticStatus::WARN, std::to_string( cnt_notok ) + "Issues " + t);
  }


  void coeMasterDiagnostics(diagnostic_updater::DiagnosticStatusWrapper&  stat )
  {
    ros::Time   n  = ros::Time::now();
    double      at = n.toSec();
    std::string st = " [" + boost::posix_time::to_simple_string( n.toBoost() ) +"]";

    coe_master::CoeMaster::MasterDiagnosticPtr&   errors = master_.getCoeDriverGenericErrors();
    size_t sz = errors->size();
    if( sz == 0 )
    {
      stat.summary(diagnostic_msgs::DiagnosticStatus::OK,  "No errors" + st );
    }
    else
    {
      bool stale = true;
      for( size_t i=0; i<sz; i++ )
      {
        const std::tuple<std::string, double, std::string > & e = errors->get().at(i);
        double et = std::get<1>( e );
        stale &= ( ( at - et ) > 10.0 );

        stat.add(std::string("Alarm ["+std::get<0>(e)+"]").c_str(), std::get<2>(e).c_str() );
      }
      stat.summary( stale ? diagnostic_msgs::DiagnosticStatus::STALE : diagnostic_msgs::DiagnosticStatus::ERROR, " Errors (last " + std::to_string( errors->size() ) + " items)" + st);
    }
  }


  void coeModuleDiagnostics(diagnostic_updater::DiagnosticStatusWrapper&  stat
                           ,coe_master::CoeMaster::ModuleDiagnosticPtr&  errors )
  {
    ros::Time   n  = ros::Time::now();
    double      at = n.toSec();
    std::string st = " [" + boost::posix_time::to_simple_string( n.toBoost() ) +"]";

    size_t sz = errors->size();
    if( sz == 0 )
    {
      stat.summary(diagnostic_msgs::DiagnosticStatus::OK,  "No errors" + st );
    }
    else
    {
      bool stale = true;
      for( size_t i=0; i<sz; i++ )
      {
        const std::tuple<std::string, double, ec_errort > & e = errors->get().at(i);
        double et = std::get<1>( e );
        stale &= ( ( at - et ) > 10.0 );

        const ec_errort & Ec = std::get<2>(e);

        std::string soem_time = " [" + std::get<0>( e ) + "] [" + coe_soem_utilities::to_string( Ec.Etype )+"]";
        switch (Ec.Etype)
        {
          case EC_ERR_TYPE_SDO_ERROR:     stat.addf(coe_core::to_string_hex((unsigned)Ec.AbortCode), "%s 0x%4.4x.%2.2x:%s", soem_time.c_str(), Ec.Index, Ec.SubIdx, ec_sdoerror2string(Ec.AbortCode) ); break;
          case EC_ERR_TYPE_EMERGENCY:     stat.addf(coe_core::to_string_hex(Ec.ErrorCode          ), "%s manufacturer Specific - See Plugin Diagnostic",soem_time.c_str());                             break;
          case EC_ERR_TYPE_PACKET_ERROR:  stat.addf(coe_core::to_string_hex(Ec.ErrorCode          ), "%s 0x%4.4x:%2.2x, Code TODO PARSER",soem_time.c_str(), Ec.Index, Ec.SubIdx );                     break;
          case EC_ERR_TYPE_SDOINFO_ERROR: stat.addf(coe_core::to_string_hex((unsigned)Ec.AbortCode), "%s 0x%4.4x:%2.2x %s", soem_time.c_str(), Ec.Index, Ec.SubIdx, ec_sdoerror2string(Ec.AbortCode));  break;
          case EC_ERR_TYPE_SOE_ERROR:     stat.addf(coe_core::to_string_hex((unsigned)Ec.AbortCode), "%s IDN 0x%4.4x %s", soem_time.c_str(), Ec.Index, ec_soeerror2string(Ec.AbortCode));               break;
          case EC_ERR_TYPE_MBX_ERROR:     stat.addf(coe_core::to_string_hex(Ec.ErrorCode          ), "%s MBX %s", soem_time.c_str(), Ec.Index, ec_mbxerror2string(Ec.ErrorCode));                       break;
          default:                        stat.addf(coe_core::to_string_hex((unsigned)Ec.AbortCode), "%s Not mapped", soem_time.c_str());                                                               break;
        }
      }

      stat.summary( stale ? diagnostic_msgs::DiagnosticStatus::STALE : diagnostic_msgs::DiagnosticStatus::ERROR, " Errors (last " + std::to_string( errors->size() ) + " items)" + st);
    }
  }

public:
  static int ctrl_c_hit_count_;

  CoeDriverNode(ros::NodeHandle& nh)
  : node_handle_(nh)
  , private_node_handle_("~")
  , diagnostic_()
  {
    ROS_INFO("Check verbosity level (%s)....... ", (private_node_handle_.getNamespace() + "/verbose").c_str()  );
    if( private_node_handle_.hasParam("debug") )
    {
      ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug);
    }

    desired_freq_ = 0;
    master_.setPostOpenHook(boost::bind(&CoeDriverNode::postOpenHook, this));
    reset_error_service_.reset( new ros::ServiceServer( node_handle_.advertiseService ( "reset_errors", &CoeDriverNode::resetErrors, this) ) );

  }

  void postOpenHook()
  {
    diagnostic_.setHardwareID(master_.getID());

    const std::map<int,std::string>& add_map = master_.getAddressUniqueIdMap();
    std::map< int, coe_master::CoeMaster::ModuleDiagnosticPtr >& master_sorted_typed_errors = master_.getCoeDriverTypedErrors();

    for( auto it=master_sorted_typed_errors.begin();it!=master_sorted_typed_errors.end();it++ )
    {
      std::string                       node_name = add_map.at( it->first );
      coe_master::CoeMaster::ModuleDiagnosticPtr&  node_typed_errors = it->second;

      diagnostic_.add ( node_name, boost::bind(&CoeDriverNode::coeModuleDiagnostics, this, _1, node_typed_errors )  );
    }
    diagnostic_.add ( "Generic Errors", boost::bind(&CoeDriverNode::coeMasterDiagnostics, this, _1)  );
    sdo_.reset( new SdoManager( master_.getNetworkDescriptor(), true ) );
  }
  
  static void hupCalled(int sig)
  {
    ROS_WARN("Unexpected SIGHUP caught. Ignoring it.");
  }
  
  static void sigCalled(int sig)
  {
    ROS_WARN("SIGTERM caught. Clean Exit procedure.");
    ctrl_c_hit_count_++;
  }
  
  int spin()
  {
    prepareDiagnostics();
    
    
    master_.goRunning();

    /// @todo Do something about exit status?
    std::string last_status_message;
    while ( node_handle_.ok() )
    {
      if( ctrl_c_hit_count_ > 0 )
      {
        ROS_WARN("Try to Kill softly...");
        if( !master_.goClosed() ) 
        {
          ROS_WARN("Kill hardly...");
        }
        break;
        
      }
      else
      {
        boost::recursive_mutex::scoped_lock lock_(master_.mutex_);
        if (!master_.isRunning())
        {             
          std::string new_status_message = master_.getStatusMessage();
          if ( (last_status_message != new_status_message || master_.getRecoveryComplete()) && !master_.getStatusOk())
          {
            ROS_ERROR("%s", new_status_message.c_str()); 
            master_.clearRecoveryComplete();
            last_status_message = new_status_message;
          }
          ros::WallDuration(1).sleep();
          master_.goClosed(); 
          master_.goRunning();
        }
        diagnostic_.update();
      }
      ros::WallDuration(0.1).sleep();
    }

    master_.goClosed();

    ros::shutdown();
    
    return 0; /// @todo Work on return type here.
  }
  
};

int CoeDriverNode::ctrl_c_hit_count_ = 0;

int main(int argc, char* argv[])
{
  if( !realtime_utilities::rt_main_init(PRE_ALLOCATION_SIZE) )
  {
    perror("Error in rt_main_init. Exit. Have you launched the node as superuser?");
    return -1;
  }
  
  ros::init(argc, argv, "eureca_master", ros::init_options::NoSigintHandler);
  ros::AsyncSpinner spinner(4);
  spinner.start();
  
  ros::NodeHandle nh;
  CoeDriverNode driver(nh);
  
  signal(SIGINT,  &CoeDriverNode::sigCalled);
  signal(SIGTERM, &CoeDriverNode::sigCalled);
  signal(SIGHUP,  &CoeDriverNode::hupCalled);

  
  return driver.spin();
  
}











