#include <iostream>
#include <fstream>
#include <ros/ros.h>


int main(int argc, char* argv[] )
{
  ros::init(argc, argv, "coe_zeros_node");
  ros::NodeHandle nh;

  std::vector<std::string> joint_ids;
  XmlRpc::XmlRpcValue list;
  if( nh.getParam("coe/module_list", list))
  {
    if( list.getType() == XmlRpc::XmlRpcValue::TypeArray )
    {
      for( size_t i=0; (long int)i<list.size(); i++ )
      {
        XmlRpc::XmlRpcValue element = list[i];
        if( element.getType() == XmlRpc::XmlRpcValue::TypeStruct )
        {
          if( element.hasMember("label") && element.hasMember("address") )
          {
            std::string label = element["label"];
            int address       = element["address"];
            joint_ids.push_back( label+"__"+std::to_string(address) );
          }
        }
      }
    }
  }
  else
  {
    ROS_WARN("The parameter '%s/coe/module_list' is not in the rosparam server. Bye bye", nh.getNamespace().c_str() );
  }

  for( const std::string& joint_id : joint_ids )
  {
    std::string last_position_file_name;
    if( nh.getParam("coe/" + joint_id + "/last_position_file_name", last_position_file_name ))
    {
      ROS_INFO("Getting zero data from '%s'", last_position_file_name.c_str() );

      int32_t last_position = 0;
      std::ifstream fn( last_position_file_name.c_str()  );
      if( fn.fail() )
      {
        ROS_ERROR("Failed in opening file: '%s'", last_position_file_name.c_str() );
      }
      else
      {
        fn >> last_position;
        ROS_INFO("Last position ofr the axis '%s' is '%d'", joint_id.c_str(),  last_position);
      }
    }
    else
    {
      ROS_INFO("Node '%s' has not any parameter for the achieved last position", (nh.getNamespace() + "/coe/" + joint_id + "/last_position_file_name") .c_str() );
    }
  }


  return 0;
}



